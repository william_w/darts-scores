# darts-scores

This module displays scores of a darts game given the x and y coordinates of the point that the dart landed on the dart board.

## Pre-requisites
You will need to have the following installed and configured appropriately on your computer.
* Nodejs
* Git
* yarn


## Setup:

### Clone the project to your local drive
1. Navigate to your projects folder.
2. Run the command `git clone git@gitlab.com:william_w/darts-scores.git` to clone the repository.

## Test and Run:

### Initialize your project
1. Open your project folder on your preferred terminal.
2. Run the command `yarn` to install the project dependencies.

    
### Running the project

1.  Run the command `yarn test` to execute all the unit tests.
2.  On your terminal, run the command `node score.js x y` to execute the application locally. With x and y being the values of the x and y coordinates respectively.
    NB: Make sure that you separate the two arguments with a space character. 

## Assumptions:

1.  If you fail to provide any coordinates, then the score will evaluate to zero.
2.  If you provide an invalid coordinates such as a alpha character or a symbol, then the score will also evaluate to zero.