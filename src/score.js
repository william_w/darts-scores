/**
 * this script is called as the entry point when running the module on terminal.
 */
const app = require('./app');
const args = process.argv.slice(2)

console.log(app.score(args[0], args[1]));