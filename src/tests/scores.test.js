const app = require('../app');

describe("Dart game tests", () => {
  // test of sample coordinates within the inner circle
  test("dart lands on the inner circle", () => { 
    expect(app.score(0, 0)).toBe(10);
    expect(app.score(1, 1)).toBe(10);
    expect(app.score(1, 0)).toBe(10);
    expect(app.score(0, 1)).toBe(10);
    expect(app.score(0, 0, 0)).toBe(10);
  });

  // test of sample random coordinates within the middle circle
  test("dart lands on the middle circle", () => {
    expect(app.score(0, 2)).toBe(5);
    expect(app.score(0, 3)).toBe(5);
    expect(app.score(0, 4)).toBe(5);
    expect(app.score(0, 5)).toBe(5);
    expect(app.score(1, 2)).toBe(5);
    expect(app.score(2, 3)).toBe(5);
    expect(app.score(5, 4)).toBe(5);
    expect(app.score(5, 5)).toBe(5);
    expect(app.score(5, 5, 5)).toBe(5);
  });

  // test of sample random coordinates within the outer circle
  test("dart lands on the outer circle", () => {
    expect(app.score(0, 6)).toBe(1);
    expect(app.score(0, 7)).toBe(1);
    expect(app.score(0, 8)).toBe(1);
    expect(app.score(0, 9)).toBe(1);
    expect(app.score(0, 10)).toBe(1);
    expect(app.score(1, 6)).toBe(1);
    expect(app.score(4, 8)).toBe(1);
    expect(app.score(7, 9)).toBe(1);
    expect(app.score(10, 10)).toBe(1);
    expect(app.score(10, 10, 10)).toBe(1);
    expect(app.score('10', '10')).toBe(1);
  });

  // test a sample of random coordinates outside the target
  test("dart lands outside the target", () => {
    expect(app.score(0, 11)).toBe(0);
    expect(app.score(1, 12)).toBe(0);
    expect(app.score(13, 0)).toBe(0);
    expect(app.score(14, 1)).toBe(0);
    expect(app.score(11, 11)).toBe(0);
    expect(app.score(11, 11, 11)).toBe(0);
  });

  // test invalid coordinates
  test("invalid coordinates provided", () => {
    expect(app.score(null, null)).toBe(0);
    expect(app.score(undefined, undefined)).toBe(0);
    expect(app.score('1', '12')).toBe(0);
    expect(app.score('a', 0)).toBe(0);
    expect(app.score(14, 'b')).toBe(0);
    expect(app.score('', 11)).toBe(0);
    expect(app.score(0, ' ')).toBe(0);
    expect(app.score(' ', ' ')).toBe(0);
    expect(app.score(0, -1)).toBe(0);
    expect(app.score(-1)).toBe(0);
    expect(app.score()).toBe(0);
  });

})