const {INNER_CIRCLE_SCORE, MIDDLE_CIRCLE_SCORE, OUTER_CIRCLE_SCORE} = require('./constants/scores');
const {INNER_CIRCLE_COORD, MIDDLE_CIRCLE_COORD, OUTER_CIRCLE_COORD} = require('./constants/coordinates');

module.exports = {
  /**
   * score function receives two coordinates (x, y) and returns
   * the score as per the given specifications. Any extra parameter 
   * will be discarded and only the first two will be considered.
   * @param {number} x
   * @param {number} y
   */
  score: (x, y) => {
    /**
     * check function evaluates a one coordinate and returns the 
     * score associated with its value.
     * @param {number} coord 
     */

    const check = coord => {
      let val;
      if(typeof coord === 'number' && coord >= 0 && coord <= INNER_CIRCLE_COORD) {
          val = INNER_CIRCLE_SCORE;
      }
      else if(coord > INNER_CIRCLE_COORD && coord <= MIDDLE_CIRCLE_COORD) {
          val = MIDDLE_CIRCLE_SCORE;
      }
      else if(coord > MIDDLE_CIRCLE_COORD && coord <= OUTER_CIRCLE_COORD) {
          val = OUTER_CIRCLE_SCORE;
      }
      else
          val = 0;
      return val;
    }

    // call the check function and pass the two coordinates one after the other while
    // ensuring that the arguments are parsed to int prior to evaluation.
    const xscore = check(parseInt(x, 10));
    const yscore = check(parseInt(y, 10));
    if (xscore === 0 || yscore === 0)
      return 0; // return 0 for coordinates that are outside the target (or invalid coordinates)
    else
      return xscore >= yscore ? yscore : xscore; // return the lowest score
  }
}